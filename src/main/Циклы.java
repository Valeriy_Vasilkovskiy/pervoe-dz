package main;

public class Циклы {

    public void task1 (){
        int finish = 99;
        int count = 0;
        int sum = 0;
        for (int start = 1; start <=finish ; start++) {
            if(start%2==0){
                count++;
                sum+=start;
            }
        }
        System.out.println(count);
        System.out.println(sum);
    }

    public void task2(int num){
        int cheek=0;
        boolean isprime = true;
        if(num == 0){
            System.out.print("Simple");
            return;
        }
        if(num < 0){
            System.out.println("Это не натуральное число");
            return;
        }
        for (int i = 2; i != num; i++) {
            if(i>4 && num%2!=0){
                break;
            }
            for (int j = num/2; j != 0; j--){
                if(cheek > 0){
                    break;
                }
                if(num==i*j){
                    isprime= false;
                    cheek++;
                    System.out.print(i);
                    System.out.print(" * ");
                    System.out.println(j + " = "+  num);
                }
                if(i*i==num || j*j ==num){
                    isprime=false;
                }
            }
            if(cheek > 0){
                break;
            }
        }
        if(isprime){
            System.out.println("Simple");
        }else{
            System.out.println("Not Simple");
        }
    }
    public void task3(int num){
        int root = 1;
        while(root * root <= num){
            root++;
        }
        System.out.println(root-1);
    }
    public void task4 (int num){
        int fackt = 1;
        for (int i = 1; i <= num; i++) {
            fackt*=i;
        }
        System.out.println(fackt);
    }
    public void task5 (int num){
        int sum = 0;
        while (num!=0){
            sum+=num%10;
            num/=10;
        }
        System.out.println(sum);
    }
    public void task6 (int num){
        int mirror = 0;
        while(num!=0){
            mirror = mirror*10 + (num%10);
            num/=10;
        }
        System.out.print(mirror);
    }
}
