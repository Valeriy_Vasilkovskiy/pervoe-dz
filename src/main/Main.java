package main;

public class Main {

    public static void main(String[] args) {
        Условные_Операторы оператор = new Условные_Операторы();
        //      оператор.task1();
        //      оператор.task2();
        //      оператор.task3();
        //      оператор.task4();
        //      оператор.task5();
        Циклы цикл = new Циклы();
        //      цикл.task1();
        //      цикл.task2(1352343533);
        //      цикл.task3(256);
        //      цикл.task4(6);
        //      цикл.task5(42);
        //      цикл.task6(1234);
        Массивы arr = new Массивы();
        //      arr.task1_3();
        //      arr.task2_4();
        //      arr.task5();
        //      arr.task6();
        //      arr.task7();
        //      arr.task8();
        //      arr.task9();
        Функции func = new Функции();
        //      func.task1(6);
        //      func.task2(134);
        //      func.task3("Сто двадцать четыре");
        func.task4(2, 2, 3, 4);
        Фигуры figure = new Фигуры();
        //      figure.triangel_down();
        //      figure.triangel_up();
        //      figure.square();
        //      figure.square_with_central_hole();
        //      figure.triangle_with_central_hole();
    }
}
