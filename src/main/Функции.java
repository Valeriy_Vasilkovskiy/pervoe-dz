package main;

public class Функции {

    public void task1(int day) {
        if (day <= 0 || day > 7) {
            System.out.println("Это число не составляет порядковый ряд дней недели");
        } else {
            switch (day) {
                case 1:
                    System.out.println(day + "- Monday");
                    break;
                case 2:
                    System.out.println(day + "- Tuesday");
                    break;
                case 3:
                    System.out.println(day + "- Wednesday");
                    break;
                case 4:
                    System.out.println(day + "- Thursday");
                    break;
                case 5:
                    System.out.println(day + "- Friday");
                    break;
                case 6:
                    System.out.println(day + "- Saturday");
                    break;
                case 7:
                    System.out.println(day + "- Sunday");
                    break;
            }
        }
    }

    public void task2(int num) {
        int num_1 = num;
        int hundred = 0;
        int dozen = 0;
        int unit = 0;
        int cheek = 0;
        String h = "";
        String d = "";
        String u = "";
        while (num != 0) {
            cheek++;
            num /= 10;
        }
        if (cheek == 3) {
            num = num_1;
            hundred = num / 100;
            num = num_1;
            dozen = num / 10 % 10;
            num = num_1;
            unit = num % 10;
        } else if (cheek == 2) {
            num = num_1;
            dozen = num / 10;
            unit = num % 10;
        } else if (cheek == 1) {
            num = num_1;
            unit = num % 100;
        }
        if (hundred != 0) {
            switch (hundred) {
                case 1:
                    h = "Сто";
                    break;
                case 2:
                    h = "Двести";
                    break;
                case 3:
                    h = "Триста";
                    break;
                case 4:
                    h = "Четыреста";
                    break;
                case 5:
                    h = "Пятьсот";
                    break;
                case 6:
                    h = "Шестьсот";
                    break;
                case 7:
                    h = "Семьсот";
                    break;
                case 8:
                    h = "Восемьсот";
                    break;
                case 9:
                    h = "Девятьсот";
                    break;
            }
        }
        ////////////////////////////////////////
        if (dozen == 0) {
            d = "";
        }
        if (dozen == 1) {
            if (unit == 0) {
                d = "Десять";
            }
            if (unit == 1) {
                d = "Одиннадцать";
            }
            if (unit == 2) {
                d = "Двенадцать";
            }
            if (unit == 3) {
                d = "Тринадцать";
            }
            if (unit == 4) {
                d = "Четырнадцать";
            }
            if (unit == 5) {
                d = "Пятнадцать";
            }
            if (unit == 6) {
                d = "Шестнадцать";
            }
            if (unit == 7) {
                d = "Семнадцать";
            }
            if (unit == 8) {
                d = "Восемнадцать";
            }
            if (unit == 9) {
                d = "Девятнадцать";
            }
        }
        ////////////////////////////
        if (dozen == 0 || dozen > 1) {
            if (dozen == 2) {
                d = "Двадцать";
            }
            if (dozen == 3) {
                d = "Тридцать";
            }
            if (dozen == 4) {
                d = "Сорок";
            }
            if (dozen == 5) {
                d = "Пятьдесят";
            }
            if (dozen == 6) {
                d = "Шестьдесят";
            }
            if (dozen == 7) {
                d = "Семьдесят";
            }
            if (dozen == 8) {
                d = "Восемьдесят";
            }
            if (dozen == 9) {
                d = "Девяносто";
            }
            //////////////////////
            if (unit == 0) {
                u = "";
            }
            if (unit == 1) {
                u = "Один";
            }
            if (unit == 2) {
                u = "Два";
            }
            if (unit == 3) {
                u = "Три";
            }
            if (unit == 4) {
                u = "Четыре";
            }
            if (unit == 5) {
                u = "Пять";
            }
            if (unit == 6) {
                u = "Шесть";
            }
            if (unit == 7) {
                u = "Семь";
            }
            if (unit == 8) {
                u = "Восемь";
            }
            if (unit == 9) {
                u = "Девять";
            }
        }
        //System.out.println(hundred+"; "+dozen+"; "+unit);
        System.out.println(num_1);
        if (cheek == 1) {
            System.out.println(u);
        } else if (cheek == 2) {
            System.out.println(d + " " + u);
        } else {
            System.out.println(h + " " + d + " " + u);
        }
    }

    public void task3(String string) {
        int h = 0;
        int d = 0;
        int d_u = 0;
        int u = 0;

        String[] string_arr = string.split("\\s");

        String[] hundred = {
                "Сто", "Двести", "Триста",
                "Четыреста", "Пятьсот", "Шестьсот",
                "Семьсот", "Восемьсот", "Девятьсот"
        };
        String[] units = {
                "один", "два", "три",
                "четыре", "пять", "шесть",
                "семь", "восемь", "девять"
        };
        String[] dozen = {
                "десять", "двадцать", "тридцать",
                "сорок", "пятьдесят", "шестьдесят",
                "семьдесят", "восемьдесят", "девяносто"
        };
        String[] dozen_unit = {
                "одиннадцать", "двенадцать", "тринадцать",
                "четырнадцать", "пятнадцать", "шестнадцать",
                "семнадцать", "восемнадцать", "девятнадцать"
        };
        int[] hundred_num = {
                100, 200, 300, 400, 500, 600, 700, 800, 900
        };
        int[] dozen_num = {
                10, 20, 30, 40, 50, 60, 70, 80, 90
        };
        int[] dozen_unit_num = {
                11, 12, 13, 14, 15, 16, 17, 18, 19
        };
        int[] unit_num = {
                1, 2, 3, 4, 5, 6, 7, 8, 9
        };

        for (int i = 0; i != string_arr.length; i++) {
            for (int j = 0; j != 9; j++) {
                if (string_arr[i].equals(hundred[j])) {
                    h = hundred_num[j];
                }
                if (string_arr[i].equals(dozen[j])) {
                    d = dozen_num[j];
                }
                if (string_arr[i].equals(dozen_unit[j])) {
                    d_u = dozen_unit_num[j];
                }
                if (string_arr[i].equals(units[j])) {
                    u = unit_num[j];
                }
            }
        }
        if (d_u != 0 && u != 0) {
            System.out.println("Неправильный ввод");
        }
        if (h != 0) {
            if (d != 0 && d_u == 0 && u != 0) {
                System.out.println(h + d + u);
            }
            if (d != 0 && d_u == 0 && u == 0) {
                System.out.println(h + d);
            }
            if (d == 0 && d_u != 0) {
                System.out.print(h + d_u);
            }
            if (d_u == 0 && d == 0 && u != 0) {
                System.out.println(h + u);
            }
            if (d == 0 && d_u == 0 && u == 0) {
                System.out.println(h);
            }

        }
        if (h == 0) {
            if (d != 0 && d_u == 0 && u != 0) {
                System.out.println(d + u);
            }
            if (d != 0 && d_u == 0 && u == 0) {
                System.out.println(d);
            }
            if (d == 0 && d_u != 0) {
                System.out.print(d_u);
            }
            if (d_u == 0 && d == 0 && u != 0) {
                System.out.println(u);
            }
            if (d == 0 && d_u == 0 && u == 0) {
                System.out.println("0");
            }
        }
    }

    public void task4(int X1, int Y1, int X2, int Y2) {
        double num = Math.sqrt((X2 * X2) - (2 * X1 * X2) + (X1 * X1) + (Y2 * Y2) - (2 * Y1 * Y2) + (Y1 * Y1));
        //Можно конечно использовать и
        //        int root = 1;
        //        while(root * root <= num){
        //            root++;
        //        }
        //Однако тогда он будет округляться
        System.out.println(num);
    }
}
