package main;

public class Фигуры {
    String element="x";

    int step = 0;

    public void triangel_down(){
        for(int i=0; i<5;i++){
            System.out.println();
            for (int j = 0; j < 5-i; j++) {
                System.out.print(element+" ");
            }
        }
    }
    public void triangel_up(){
        for(int i=0; i<5;i++){
            System.out.println();
            for (int j = 5; j > 5-i ; j--) {
                System.out.print(element+" ");
            }
        }
    }
    public void square(){
        for(int i=0; i<5; i++){
            System.out.println();
            for(int j=0;j<5; j++){
                System.out.print(element+" ");
            }
        }
    }
    public void square_with_central_hole(){
        for(int i=0;i<5; i++) {
            if (i == 0 || i == 4) {
                for (int j = 0; j < 5; j++) {
                    System.out.print(element + " ");
                }
                System.out.println();
            }else {
                System.out.print(element + "       " + element);
                System.out.println();
            }
        }
    }
    public void triangle_with_central_hole(){
        for(int i=0; i<5;i++) {
            if (i == 0 || i == 4) {
                for (int j = 0; j < 4 - i; j++) {
                    System.out.print(element+" ");
                }
                System.out.print(element + " ");
                System.out.println();
            }else {
                String[] space = {
                        "  ",
                        "  ",
                        "  ",
                };
                System.out.print(element);
                for (int j = 0; j < space.length - step; j++) {
                    System.out.print(space[j]);
                }
                step++;
                System.out.print(element);
                System.out.println();
            }
        }
    }
}
