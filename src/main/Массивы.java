package main;
import java.util.Random;


public class Массивы {

    int [] arr = new int [5];
    Random rand = new Random();


        public void fill_array(){
            for (int i = 0; i < arr.length; i++) {
                arr[i] = rand.nextInt(99+1);
            }
        }


        public void out_array(){
            for (int i = 0; i <arr.length ; i++) {
                System.out.print(arr[i]+"; ");
            }
            System.out.println();
        }


        public void task1_3(){
            fill_array();
            out_array();
            int indexMin = 0;
            int minimum = arr[0];
            for (int i = 0; i != arr.length; i++) {
                if(arr[i] < minimum) {
                    minimum = arr[i];
                    indexMin = i;
                }
            }
            indexMin++;
            System.out.println();
            System.out.println(minimum +"- min; index- " + indexMin);
        }
        public void task2_4(){
            fill_array();
            out_array();
            int indexMax = 0;
            int maximum = arr[0];
            for (int i = 0; i != arr.length; i++) {
                if(arr[i] > maximum) {
                    maximum = arr[i];
                    indexMax = i;
                }
            }
            indexMax++;
            System.out.println();
            System.out.println(maximum +"- max; index- " + indexMax);
        }

        public void task5(){
            int sum = 0;
            fill_array();
            out_array();
            for (int i = 0; i != arr.length; i++) {
                if(i%2!=0) {
                    sum += arr[i];
                    System.out.print(arr[i]);
                }
                if(i%2==0 || i==arr.length-2){
                    System.out.print("");
                }else{
                    System.out.print("+");
                }
            }
            System.out.println("=" + sum);
        }
        public void task6 (){
            fill_array();
            out_array();
            for (int i = 0; i != arr.length/2; i++){
                int temp = arr[i];               //Тут мы в переменной temp сохраняем значение arr[i],
                arr[i] = arr[arr.length-1-i];    //затем находим элемент симетричный ему относительно середины
                arr[arr.length-1-i] = temp;      //длинны массива и с помощью arr.length-1-i находим индекс того самого
                //симетричного элемента для i, как итог цикл повторяется минимальное
                //количество раз и с большой эффективностью.
            }
            out_array();
        }
        public void task7(){
            int amount = 0;
            fill_array();
            out_array();
            for (int i = 0; i !=arr.length ; i++) {
                if(arr[i]%2!=0){
                    amount++;
                    System.out.print(arr[i]+ " ");
                }
            }
            System.out.println();
            System.out.println("Количество нечётных за индексом элементов = " + amount);
        }
        public void task8 (){
            fill_array();
            out_array();
            for (int i = 0; i < arr.length/2; i++) {
                int temp = arr[i];
                arr[i] = arr[arr.length/2 +1+i];
                arr[arr.length/2+1+i] = temp;
            }
            out_array();
        }
        public void task9(){
            System.out.println("Bubble");
            fill_array();
            out_array();
            for (int i = 0; i <arr.length-1 ; i++) {
                for (int j = 0; j < arr.length-i-1; j++) {
                    if (arr[j] > arr[j + 1]) {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            out_array();

            System.out.println("Select");
            fill_array();
            out_array();
            for (int min = 0; min < arr.length-1; min++) {
                int minimal = min;
                for (int j = min+1; j <arr.length ; j++) {
                    if(arr[j]<arr[minimal]){
                        minimal=j;
                    }
                }
                int temp = arr[min];
                arr[min]=arr[minimal];
                arr[minimal]=temp;
            }
            out_array();

            System.out.println("Insert");
            fill_array();
            out_array();
            for (int i = 1; i < arr.length; i++) {    //Создаём переменную key в которой хранится
                int key = arr[i];                //значение элемента массива начиная с первого
                int j=i-1;                  //После чего переменная j выступает в роли дополнительного
                while (j>=0 && arr[j]>key){ //счётчика для конструкции while который и вставляет
                    arr[j+1]=arr[j];        //выбраный циклом элемент в предыдущий слот буквально двигая
                    j-=1;                   //их с помощью "вставки"
                }
                arr[j+1]= key;
            }
            out_array();
        }

    }
