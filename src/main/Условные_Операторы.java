package main;

public class Условные_Операторы {

    public void task1 (int a, int b){
        System.out.println((a%2==0)?a*b:a+b);
    }

    public void task2 (float x, float y){
        if(x==0 && y== 0){
            System.out.println("Начало координат");
        }
        if(x==0 && y>0 || y<0){
            System.out.println("Ось ОУ");
        }
        if(x>0 || x<0 && y==0){
            System.out.println("Ось ОХ");
        }
        if(x>0 && y >0) {
            System.out.println("First quarter;");
        }else if (x < 0 && y < 0){
            System.out.println("Third quarter;");
        }
        if(x<0 && y > 0){
            System.out.println("Second quarter;");
        }else if(x > 0 && y <0){
            System.out.println("Fourth quarter;");
        }
    }
    public void task3 (int a, int b, int c){
        int sum = 0;
        if(a>0){
            sum+=a;
        }
        if(b>0) {
            sum += b;
        }
        if(c>0){
            sum+=c;
        }
        System.out.println(sum);
    }
    public void task4 (int a, int b, int c){
        int max ;
        int sum = (a + b + c)+3;
        int multiplex = a*b*c;
        if (sum>multiplex){
            max=sum;
        }else{
            max = multiplex;
        }
        System.out.println(max);
    }
    public void task5 (int a){
        String rating = null;
        if(a<0 || a>100){
            System.out.println("Ошибка ввода;");
            return;
        }
        if(a<=19){
            rating = "F";
        }
        if(a>=20 && a<=39){
            rating = "E";
        }
        if(a>=40 && a<=59){
            rating = "D";

        }if(a>= 60 && a<=74){
            rating = "C";
        }
        if(a>= 75 && a<=89){
            rating = "B";
        }
        if(a>= 90) {
            rating = "A";
        }
        System.out.println(rating);
    }
}
